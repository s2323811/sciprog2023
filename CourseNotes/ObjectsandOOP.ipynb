{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "a0550d94",
   "metadata": {},
   "source": [
    "# Objects and Object Oriented Programming\n",
    "\n",
    "The next level of program design and structure includes the concepts of\n",
    "*Objects* and *Object Oriented Programming* (OOP). You have been using\n",
    "*Objects* in your code, for example in [Plotting and Graphics](plotting.ipynb), they now need some explanation.\n",
    "\n",
    "Up to now the `functions` that you have been writing take a number of arguments, do a fixed calculation based on these arguments and return a *value* or a `list` of *values*. This means that functions have no *local internal state*, so they do the same thing every time they are called. This is where *objects* differ, they have a *local internal state*, so have local internal variables that retain their values. This sounds a bit abstract, and a fine point, but it does result in a totally new way of thinking about programming; that is the fundamental difference in OOP.\n",
    "\n",
    "\n",
    "## A three-dimensional `Vector` class\n",
    "\n",
    "Let us consider that we want to build a `class` that implements\n",
    "three-dimensional vectors. We would like to write:\n",
    "\n",
    "```python\n",
    "<.....> \n",
    "vf = Vector() \n",
    "vf.setComponents(3.0, 6.0, 2.0) \n",
    "mag = vf.magnitude() \n",
    "....\n",
    "```\n",
    "\n",
    "where we create a `Vector()` object, set its components and then “ask” it for its magnitude.\n",
    "\n",
    "To do this we require that,\n",
    "\n",
    "-   The `Vector()` object has *internal variables*, in particular the three components `x,y,z` so it has a *local internal state*.\n",
    "\n",
    "-   There is a *method* called `setComponents()` that can set the *local internal state* to the specified value.\n",
    "\n",
    "-   There is a *method* called `magnitude()` that can read this *local internal state*, calculate the magnitude and return it.\n",
    "\n",
    "Now lets look at the `Python` code that will do this,\n",
    "\n",
    "```python\n",
    "class Vector(object): # Define a vector object\n",
    "    \n",
    "    def setComponents(self , xin , yin, zin): \n",
    "        self.x = float(xin) \n",
    "        self.y = float(yin) \n",
    "        self.z = float(zin)\n",
    "    \n",
    "    def magnitude(self): \n",
    "        return math.sqrt(self.x**2 + self.y**2 + self.z**2)\n",
    "\n",
    "def main(): \n",
    "    vf = Vector() \n",
    "    <...>\n",
    "```\n",
    "\n",
    "So looking at the code, then\n",
    "\n",
    "1.  The `class Vector(object): ` defines the class `Vector()`.\n",
    "\n",
    "2.  The `def setComponents(self, xin, yin, zin): ` defines a *method* that belongs to the class `Vector` that takes *three* arguments.  \n",
    "    **Note** the `self` keyword defines it as a *method* belonging to the class and is not an argument.\n",
    "\n",
    "    This *method* takes the three arguments and sets *three* internal\n",
    "    variables called `self.x`, `self.y` and `self.z`, where the prefix specifies that they are internal variables belonging to the `Vector` class.\n",
    "\n",
    "3.  The `def magnitude(self): ` defines a *method*, again belonging to the `Vector` class, that reads the three internal variables, calculates the magnitude of the vector and returns the value.\n",
    "\n",
    "    **Note** again the `self` in the definition, this specifies that `magnitude()` is a method belonging to `Vector`.\n",
    "\n",
    "    **Note** also the internal variables are accessed with `self.x`, `self.y`, `self.z`. \n",
    "\n",
    "The previous code `vf = Vector()` etc, will now work, but is not very *elegant*.\n",
    "\n",
    "\n",
    "## The constructor\n",
    "\n",
    "In the example above there were two calls, one to create the `Vector` and one to set its values. To do the both at the same time we have to add a special *method* called a *constructor* that is automatically called when the `Vector` object is created. The constructor has the complex name of `__init__()`. The code then becomes:\n",
    "\n",
    "```python\n",
    "class Vector(object):\n",
    "    \n",
    "    def __init__(self, xin, yin, zin): \n",
    "        self.x = float(xin) \n",
    "        self.y = float(yin) \n",
    "        self.z = float(zin)\n",
    "    \n",
    "    <.....>\n",
    "\n",
    "def main(): \n",
    "    vf = Vector(12,24,-0.34) \n",
    "    <....>\n",
    "```\n",
    "\n",
    "Now what happens is that when we create a `Vector` with arguments the constuctor *method* is called automatically, which in this cases sets the three internal variables `self.x`, `self.y`, `self.z`,  so starting to build a more useful `class`.\n",
    "\n",
    "> ### More special methods \n",
    "There are *many* other special methods, sometimes know as *magic methods*, all of which start and end with `\"__\"`, which implement a series of automated operations on classes. This is a particularly *messy* bit of `Python`-OOP and is best left until you really understand what you are doing. The full documentation can be found at:\n",
    "\n",
    "-  [Python Data Model](https://docs.python.org/3/reference/datamodel.html) \n",
    "\n",
    "but it does not make easy reading.\n",
    "\n",
    "## Adding an `add`\n",
    "\n",
    "To carry on making a useful class, let us now implement an `add()` method which will allow us to write\n",
    "\n",
    "```python\n",
    "<...> \n",
    "va = Vector(34, 17, 19) \n",
    "vb = Vector(-16, 2.56, -12.34) \n",
    "vc = va.add(vb) \n",
    "<....>\n",
    "```\n",
    "where `vc` will be a new `Vector` that is $va+vb$. To implement this we need to add a new *method* to the definition of `Vector`, being:\n",
    "\n",
    "```python\n",
    "class Vector(object): \n",
    "    <.....>           # other classes, eg \\_\\_init\\_\\_\n",
    "    \n",
    "    def add(self , b): \n",
    "        x = self.x + b.x \n",
    "        y = self.y + b.y \n",
    "        z = self.z + b.z\n",
    "        return Vector(x,y,z)\n",
    "```\n",
    "\n",
    "There is a some new syntax here, so lets look in detail,\n",
    "\n",
    "1.  The `def add(self , b): ` defines *method* of `Vector` that takes a single argument `b`.\n",
    "\n",
    "    **Note** again that `self` is not an aurgument, is specifies that `add` is a *method* of `Vector`.\n",
    "\n",
    "2.  Inside the `add()` method we access the internal variables `self.x` etc. and also the internal variables of the argument `Vector b`, where `b.x` is the internal variable `self.x` in `b`.\n",
    "\n",
    "3.  We create three temporary `float`s being `x,y,z` which are local to the `.add()` method *only*.\n",
    "\n",
    "4.  Then we `return` a *new* `Vector` with its internal variables set to `x,y,z`.\n",
    "\n",
    "This all looks over complicated for a simple *add*, but lets now consider taking this process forward; we can write a whole series of\n",
    "*methods* of the `Vector`, for example to form `.dot()` and `.cross()` products, all of which are *built-into* the `Vector` class. So in your `main()` program you can write\n",
    "\n",
    "```python\n",
    "<......> \n",
    "torque = distance.cross(force) \n",
    "<......>\n",
    "```\n",
    "where `torque`,`distance` and `force` are all `Vector`s.\n",
    "\n",
    "> ### Key Point\n",
    "The advantage here is now:\n",
    "-   The code in the `main()` program is shorter and easier to understand,\n",
    "-   The details of how to take a cross-product is *built-into* `Vector` class *once and for all*, so that you can use the same expression whether your are calculating torque, angular momentum, dipole moments, current flow, fluid flow etc, and you *know*, that once you have debugged it in one application then it *will* work in others.\n",
    "\n",
    "\n",
    "## Using this throughout the course\n",
    "\n",
    "You have in fact been using this OOP idea thoughout the course, when\n",
    "working with `Basic Variables` you used the syntax\n",
    "\n",
    "```python\n",
    "z = complex(10,10) \n",
    "a = z.real \n",
    "b = z.imag \n",
    "<....>\n",
    "```\n",
    "so clearly `complex()` is a `class` with internal variables `.real` and `.imag`.\n",
    "\n",
    "Also to plot a graph you wrote\n",
    "```python\n",
    "plt.show()\n",
    "```\n",
    "which activates a *method* to `.show()` the graph on your screen.\n",
    "\n",
    "\n",
    "## What Next\n",
    "\n",
    "Objected Oriented Programming (OOP) is used extensively in `Python` *but* most users make use of pre-written classes from standard libraries rather than writing their own. However, to use these well it is useful to have an understanding of what is actually happening.\n",
    "\n",
    "The way OOP is implemented in `Python` makes writing classes from scratch *hard work*, there are also many *traps* and unexpected *features* that even very experienced programmers fall foul of; hence we have covered only the very basics here. However OOP is fundamental to good modern program design and something that anybody wishing to learn *good computing style* will have to eventually master.\n",
    "\n",
    "\n",
    "> ### Other Languages \n",
    "Most other modern computer laungaues include OOP, the main two being:\n",
    "-   `Java` where essentially *everything is a class*, here you must use *classes* for everything. This makes simple programs overly complicated and the language conceptually difficult for novice programmers. It does however offer a very logical and complete OOP environment making it ideal for large complex applications.\n",
    "-   `C++` which (in my opinion) has an even more confusing OOP environment than `Python` with even more scope for *getting it wrong*. Again like `Python` most users make extensive use of OOP pre-written classes, for example by using template libraries, but a few write new classes from scratch.\n",
    "\n",
    ">> **Code Example:**\n",
    "-   More developed `Vector3d` class to play with : [Vector](../CodeExamples/Vector.ipynb)\n",
    "\n",
    ">> This class starts to develop the OOP ideas, but is nowhere near\n",
    "*complete*; I have written a *full* Vector2D and Vector3D class that\n",
    "fully implements all useful methods, but this runs to 1,400 lines\n",
    "including comments.\n",
    "\n",
    ">> This shows that OOP is *very powerful* but there is a significant\n",
    "overhead in getting started.\n",
    "\n",
    "\n",
    "## What more is there?\n",
    "\n",
    "There are a number of aspects of OOP we have not touched on, the most\n",
    "important being: \n",
    "\n",
    "-   `Inheritance` where a new class can *extend* current class(s) so\n",
    "    *inheriting* its / their local variables and methods; this allows\n",
    "    you to to build up complex classes but re-using existing classes.\n",
    "\n",
    "-   `Method Overload` where a *method* in extending classes has the same\n",
    "    name as in the base class and *overloads* (or replaces) the original\n",
    "    method. This sounds more obscure than it is and is natural thing\n",
    "    when you start using classes.\n",
    "\n",
    "-   `Operator Overload` where you can use special (or magic) methods to\n",
    "    refine the arithemtic and logical operators `\"+\", \"-\", \"*\"` etc, to apply to classes, so for example in the above `Vector` class if we redefine `\"+\"` we can write\n",
    "    ```python\n",
    "    a = Vector(1,2,3) \n",
    "    b = Vector(3,4,7) \n",
    "    c = a + b\n",
    "    ```\n",
    "    and it does just what you expect.\n",
    "\n",
    "  \n",
    "> ### How\n",
    "This one is actually easy, the additional method is:\n",
    "```python\n",
    "def __add__(self, b): \n",
    "    x = self.x + b.x \n",
    "    y = self.y + b.y \n",
    "    z = self.z + b.z \n",
    "    return Vector(x,y,z)\n",
    "```\n",
    "> This is a very powerful but *dangerous* feature of `Python`, the scope for trying to be *too clever* is totally unlimited; use with extreme care until you really understand what you are doing!\n",
    "\n",
    "There are many other complex issues associated with scope of variables\n",
    "and how classes are passed as arguments to methods, all of which you\n",
    "will learn either in future courses or more commonly, by *experimenting*. Often\n",
    "*getting it wrong* and having to work out for youself what is *really\n",
    "happening* or “Googling” the unexpected error! That is how we *learn*.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
